var rock = {
    name: "ROCK"
};
var paper = {
    name: "PAPER"
};
var scissors = {
    name: "SCISSORS"
};
var CHOICE = [rock.name, paper.name, scissors.name];

var playerScore = 0;
var botScore = 0;
var playerScoreTotal = 0;
var botScoreTotal = 0;
var tieScore = 0;
var totalGamesCounter = 0;
var tie = 'TIE';
var win = 'WIN';
var lose = 'LOSE';

function clickHandle(playerChoice) {
    var botChoice = generateBotChoice();
    showBotsResultImage(botChoice);
    var gameResult = getGameResult(playerChoice, botChoice);
    updateGameScore(gameResult);
    setTimeout(function () {
        $("#ROCK").css("display", "none");
        $("#SCISSORS").css("display", "none");
        $("#PAPER").css("display", "none");
    }, 1000);
}

function generateBotChoice() {
    var randomValue = Math.floor(Math.random() * CHOICE.length);
    return CHOICE[randomValue];
}

function showBotsResultImage(botChoice) {
    var rock = $("#ROCK").attr("id");
    var scissors = $("#SCISSORS").attr("id");
    var paper = $("#PAPER").attr("id");
    if (botChoice === rock) {
        $("#ROCK").css("display", "block");
    } else if (botChoice === scissors) {
        $("#SCISSORS").css("display", "block");
    } else if (botChoice === paper) {
        $("#PAPER").css("display", "block");
    }
}

function updateGameScore(gameResult) {
    showTextResult = "";
    if (gameResult === win) {
        playerScore++;
        playerScoreTotal =+ playerScore;
        updatePlayersWins(playerScore);
        updatePlayersWinsStat(playerScore);
        showTextResult = "Hurray! You won!";
        updateTotalGameScore(showTextResult);
    }

    if (gameResult === lose) {
        botScore++;
        botScoreTotal =+ botScore;
        updateComputerWins(botScore);
        updateComputerWinsStat(botScore);
        showTextResult = "You lose:(";
        updateTotalGameScore(showTextResult);
    }
    
    if (gameResult == tie) {
        tieScore++;
        tieScoreTotal =+ tieScore;
        updateGameCounterBoxTie(tieScore);
        showTextResult = "Draw!";
        updateTotalGameScore(showTextResult);
    }

    if (playerScore > botScore) {
        $("#lblMsg").text("You keep the score! Well done!");
    } else if (playerScore < botScore) {
        $("#lblMsg").text("You are inferior to the computer. Don't give up, keep playing!");
    } else {
        $("#lblMsg").text("So far it's a draw. Play again.");
    }
    updateGameBoxCounter(totalGamesCounter);
}

function getGameResult(playerChoice, botChoice) {
    if (playerChoice === botChoice)
        return tie;
    switch (playerChoice) {
        case rock.name:
            return botChoice === scissors.name ? win : lose;
        case scissors.name:
            return botChoice === paper.name ? win : lose;
        case paper.name:
            return botChoice === rock.name ? win : lose;
    }
}

function updateComputerWins(botScore) {
    $('#computerWinCounts').text(botScore);
}

function updatePlayersWins(playerScore) {
    $('#playersWinCounts').text(playerScore);
}

function updateComputerWinsStat(botScore) {
    $('#computerWinCountsStat').text(botScore);
}

function updatePlayersWinsStat(playerScore) {
    $('#playersWinCountsStat').text(playerScore);
}

function updateGameCounterBoxTie(tieScore) {
    $('#gameCntBoxTie').text(tieScore);
}

function updateTotalGameScore(showTextResult) {
    $('#counterRes').text(showTextResult, totalGamesCounter++);
}

function updateGameBoxCounter(totalGamesCounter) {
    $('#gameBoxCounter').text(totalGamesCounter);
}