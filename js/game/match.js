function refreshGamesEachSecond() {
    setInterval(refreshGames, 5000);
    setInterval(function () {
        RpsApi.getGameStatus()
            .then(gameStatus => {
                let gameStatusObject = JSON.parse(gameStatus);
                if (gameStatusObject.opponentName !== null || gameStatusObject.opponentName !== '') {
                    localStorage.setItem('opponentsName', gameStatusObject.opponentName);
                } else {
                    localStorage.setItem('opponentsName', 'Anonymous');
                }
            });
        $('#userName').text(localStorage.getItem('userName'));
        $('#opponentsName').text(localStorage.getItem('opponentsName'));
    }, 2000);
}

function refreshGames() {
    RpsApi.allGames()
        .then(games => {
            if (games.length === 0) {
                document.getElementById('games').innerHTML = "inga spel tillgängliga";
                return;
            }
            let gamesListHtml = games.map(game => {
                return createGameRow(game.gameId, game.name);
            }).join();
            document.getElementById('games').innerHTML = gamesListHtml;
        });
}

function createGameRow(gameId, name) {
    if (name == null || name == 'null')
        name = 'Namnlöst spel';
    return '<li class="list-group-item list-group-warning">\n' +
        '<p>' + name + '</p>\n' + '<button id = "btn-find-opponent" onclick = "joinGame(\'' + gameId + '\' , \'' + name + '\')" class="styled-button-1">' + "Join" + '</button>\n' +
        '</li>\n' +
        '<hr/>'
}

function joinGame(gameId, opponentsName) {
    if (opponentsName == null || opponentsName === '') {
        opponentsName = 'Anonymous';
    } else {
        localStorage.clear();
        localStorage.setItem('opponentsName', opponentsName);
    }
    RpsApi.setName(userName)
        .then(ignore => {
            RpsApi.joinGame(gameId)
                .then(gameStatus => {
                    window.location.href = "startGame.html"
                })
        })
}

function startRemoteGame() {
    let userName = document.getElementById('game-user-name').value;
    if (userName == null || userName === '') {
        userName = 'Anonymous';
    } else {
        if (localStorage.getItem("opponentsName") === null) {
            localStorage.setItem('opponentsName', "Oppopnent är inte kopplat än...");
        }
        localStorage.setItem('userName', userName);
    }
    RpsApi.setName(userName)
        .then(ignore => {
            RpsApi.newGame()
                .then(gameStatus => {
                    localStorage.setItem('gameId', gameStatus.gameId);
                    window.location.href = 'startGame.html'
                });
        })
}

refreshGamesEachSecond();