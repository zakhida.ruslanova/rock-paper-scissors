const RpsApi = {
    token: '',
    //setToken: (token) => { sessionStorage.setItem('token', token);},/ samma som rad 4
    //setToken: (token) => sessionStorage.setItem('token', token),
    // getToken: () => {return sessionStorage.getItem('token');},/ samma som rad 6
    getToken: () => sessionStorage.getItem('token'),

    newToken: () => {
        fetch('https://java19.sensera.se/auth/token')
            .then(response => response.text())
            //.then(text => this.setToken(text))
            .then(text => { JSON.stringify(text)
                sessionStorage.setItem('token', text)})// samma som (rad 11 + rad 4)
    },

    allGames: () => {
        //return fetch ('https://java19.sensera.se/games', {headers: {'token': this.getToken()}})
        return fetch('https://java19.sensera.se/games', {headers: {'token': sessionStorage.getItem('token')}}) // samma som rad 16
            .then(response => response.json())
    },

    newGame: () => {
        return fetch('https://java19.sensera.se/games/start', {headers: {'token': sessionStorage.getItem('token')}})
            .then(response => response.json())
    },

    joinGame: (gameId) => {
        return fetch('https://java19.sensera.se/games/join/' + gameId, {headers: {'token': sessionStorage.getItem('token')}})
            .then(response => response.json())
    },

    setName: (name) => {
        return fetch('https://java19.sensera.se/user/name', {method: 'POST', body: JSON.stringify({"name": name}), headers: {'Accept': 'application/json', 'Content-Type': 'application/json','token': sessionStorage.getItem('token')}})
            .then(response => response.text())
    },

    getGameStatus: () => {
        return fetch('https://java19.sensera.se/games/status', {headers: {'token': sessionStorage.getItem('token')}})
            .then(response => response.text())
    },

    makeMove: (sign) => {
        return fetch('https://java19.sensera.se/games/move/' + sign, {headers: {'token': sessionStorage.getItem('token')}})
            .then(response => response.text())
    },

};
// om token finns ny token inte hämtas
if (sessionStorage.getItem('token') == null) {
    RpsApi.newToken();
}
