var rock = {
    name: "ROCK"
};
var paper = {
    name: "PAPER"
};
var scissors = {
    name: "SCISSORS"
};
var CHOICE = [rock.name, paper.name, scissors.name];

var playerScore = 0;
var opponentScore = 0;
var playerScoreTotal = 0;
var opponentScoreTotal = 0;
var tieScore = 0;
var totalGamesCounter = 0;
var tie = 'TIE';
var win = 'WIN';
var lose = 'LOSE';

function clickHandle(playerChoice) {
    RpsApi.getGameStatus()
        .then(gameStatus => {
            let gameStatusObject = JSON.parse(gameStatus);
            showOpponentsResultImage(gameStatusObject.opponentMove);
        });
    RpsApi.makeMove(playerChoice)
        .then(gameStatus => {
            let gameStatusObject = JSON.parse(gameStatus);
            if(gameStatusObject.opponentMove !== '') {
                var gameResult = getGameResult(gameStatusObject.move, gameStatusObject.opponentMove);
                updateGameScore(gameResult);
                setTimeout(function () {
                    $("#ROCK").css("display", "none");
                    $("#SCISSORS").css("display", "none");
                    $("#PAPER").css("display", "none");
                }, 1000);
            } else{
                alert("Opponent har inte gjort val än!")
            }
        });
}

function showOpponentsResultImage(opponentsChoice) {
    var rock = $("#ROCK").attr("id");
    var scissors = $("#SCISSORS").attr("id");
    var paper = $("#PAPER").attr("id");
    if (opponentsChoice === rock) {
        $("#ROCK").css("display", "block");
    } else if (opponentsChoice === scissors) {
        $("#SCISSORS").css("display", "block");
    } else if (opponentsChoice === paper) {
        $("#PAPER").css("display", "block");
    }
}

function updateGameScore(gameResult) {
    showTextResult = "";
    if (gameResult === win) {
        playerScore++;
        playerScoreTotal = +playerScore;
        updatePlayersWins(playerScore);
        updatePlayersWinsStat(playerScore);
        showTextResult = "Hurray! You won!";
        updateTotalGameScore(showTextResult);
    }

    if (gameResult === lose) {
        opponentScore++;
        opponentScoreTotal = +opponentScore;
        updateComputerWins(opponentScore);
        updateComputerWinsStat(opponentScore);
        showTextResult = "You lose:(";
        updateTotalGameScore(showTextResult);
    }

    if (gameResult == tie) {
        tieScore++;
        tieScoreTotal = +tieScore;
        updateGameCounterBoxTie(tieScore);
        showTextResult = "Draw!";
        updateTotalGameScore(showTextResult);
    }

    if (playerScore > opponentScore) {
        $("#lblMsg").text("You keep the score! Well done!");
    } else if (playerScore < opponentScore) {
        $("#lblMsg").text("You are inferior to the computer. Don't give up, keep playing!");
    } else {
        $("#lblMsg").text("So far it's a draw. Play again.");
    }
    updateGameBoxCounter(totalGamesCounter);
}

function getGameResult(playerChoice, opponetsChoice) {
    if (playerChoice === opponetsChoice)
        return tie;
    switch (playerChoice) {
        case rock.name:
            return opponetsChoice === scissors.name ? win : lose;
        case scissors.name:
            return opponetsChoice === paper.name ? win : lose;
        case paper.name:
            return opponetsChoice === rock.name ? win : lose;
    }
}

function updateComputerWins(opponentsScore) {
    $('#computerWinCounts').text(opponentsScore);
}

function updatePlayersWins(playerScore) {
    $('#playersWinCounts').text(playerScore);
}

function updateComputerWinsStat(opponentsScore) {
    $('#computerWinCountsStat').text(opponentsScore);
}

function updatePlayersWinsStat(playerScore) {
    $('#playersWinCountsStat').text(playerScore);
}

function updateGameCounterBoxTie(tieScore) {
    $('#gameCntBoxTie').text(tieScore);
}

function updateTotalGameScore(showTextResult) {
    $('#counterRes').text(showTextResult, totalGamesCounter++);
}

function updateGameBoxCounter(totalGamesCounter) {
    $('#gameBoxCounter').text(totalGamesCounter);
}