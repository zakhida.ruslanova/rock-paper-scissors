FROM nginx

RUN mkdir /usr/share/nginx/html/css
RUN mkdir /usr/share/nginx/html/fonts
RUN mkdir /usr/share/nginx/html/img
RUN mkdir /usr/share/nginx/html/js
RUN mkdir /usr/share/nginx/html/js/game
RUN mkdir /usr/share/nginx/html/js/lib
RUN mkdir /usr/share/nginx/html/html

COPY css/* /usr/share/nginx/html/css
COPY fonts/* /usr/share/nginx/html/fonts
COPY img/* /usr/share/nginx/html/img
COPY js/* /usr/share/nginx/html/js
COPY js/game/* /usr/share/nginx/html/js/game
COPY js/lib/* /usr/share/nginx/html/js/lib
COPY html/* /usr/share/nginx/html/html

COPY index.html /usr/share/nginx/html/


